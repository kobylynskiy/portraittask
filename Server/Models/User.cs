﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AspCoreServer.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Image {get;set;}
    }
}
