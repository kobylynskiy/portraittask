﻿import { Component, Input, ViewChild, Type, OnInit, NgZone } from '@angular/core';
import { IUser } from '../../models/User';
import { UserService } from '../../shared/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {ImageCropperComponent, CropperSettings, Bounds} from 'ng2-img-cropper';

@Component({
    selector: 'user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent{
    user : IUser;
    id : any;
    data2:any;
    edit: boolean;
    cropperSettings2:CropperSettings;
    @ViewChild('cropper', undefined) cropper:ImageCropperComponent;
    constructor(private zone :NgZone,private userService: UserService,private route: ActivatedRoute) 
    {
        //Cropper settings 2
        this.cropperSettings2 = new CropperSettings();
        this.cropperSettings2.width = 200;
        this.cropperSettings2.height = 200;
        this.cropperSettings2.keepAspect = false;

        this.cropperSettings2.croppedWidth = 200;
        this.cropperSettings2.croppedHeight = 200;

        this.cropperSettings2.canvasWidth = 500;
        this.cropperSettings2.canvasHeight = 300;

        this.cropperSettings2.minWidth = 100;
        this.cropperSettings2.minHeight = 100;

        this.cropperSettings2.rounded = false;
        this.cropperSettings2.minWithRelativeToResolution = false;

        this.cropperSettings2.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
        this.cropperSettings2.cropperDrawSettings.strokeWidth = 2;
        this.cropperSettings2.noFileInput = true;

        this.data2 = {};
        this.id = this.route.snapshot.url[1].path;
        this.userService.getUsers().subscribe(result => {
            console.log('Get user result: ', result);
            console.log('TransferHttp [GET] /api/users/allresult', result);
            result as IUser[];
            this.user = result[this.id-1];
             console.log(this.user);
        });
    }

    updateUser(user) {
        user.image = this.data2.image;
        //console.log(this.data2.image);
        this.userService.updateUser(user).subscribe(result => {
            console.log('Put user result: ', result);
        }, error => {
            console.log(`There was an issue. ${error._body}.`);
        });
        this.zone.runOutsideAngular(() => {
                        location.reload();
                    });
    }

    cropped(bounds:Bounds) {
        //console.log(bounds);
    }
    onEdit(){
        this.edit = true;
    }
    onDelete(user){
        user.image = "";
         this.userService.updateUser(user).subscribe(result => {
            console.log('Put user result: ', result);
        }, error => {
            console.log(`There was an issue. ${error._body}.`);
        });
    }
    ngOnInit(){
    }
    /**
     * Used to send image to second cropper
     * @param $event
     */
    fileChangeListener($event) {
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        myReader.onloadend = function (loadEvent:any) {
            image.src = loadEvent.target.result;
            that.cropper.setImage(image);

        };

        myReader.readAsDataURL(file);
    }

    fileChange($event) {
        var image:any = new Image();
        var file:File = $event.target.files[0];
        var myReader:FileReader = new FileReader();
        var that = this;
        myReader.onloadend = function (loadEvent:any) {
            image.src = "assets/image.jpg";
            that.cropper.setImage(image);

        };

        myReader.readAsDataURL(file);
    }

    uploadPhoto($event) {
        const image = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const scope = this;

        if (!file) {
        return;
        }

        myReader.onloadend = function (loadEvent: any) {
            image.src = loadEvent.target.result;
            scope.cropper.setImage(image)
        };

        myReader.readAsDataURL(file);
    }

}
